@Library('main@main') _
pipeline {
    agent any
    stages {
        stage('Even Stage') {
            steps {
                script{
                    def a = readJSON file: 'pipeline.json'
                    evenOrOdd(currentBuild.getNumber())
                }
            }
        }
    }
}
