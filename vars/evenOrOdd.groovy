// vars/evenOrOdd.groovy

def call(int buildNumber) {
    if (buildNumber % 2 == 0) {
        pipeline {
            agent any
            stages {
                stage('Even Stage') {
                    steps {
                        script{
                            def props = readJSON file: 'pipeline.json'
                            echo "${props.name}"
                        }
                    }
                }
            }
        }
    } else {
        pipeline {
            agent any
            stages {
                stage('Odd Stage') {
                    steps {
                        script{
                            echo "The build number is odd"
                        }
                    }
                }
            }
        }
    }
}